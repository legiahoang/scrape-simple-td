console.log("App running");
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');
const schedule = require('node-schedule');
const Data = require('./models/data');

async function run() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://banggia2.ssi.com.vn');
    const textContent = await page.evaluate(() => document.querySelector('#tdHose30VnIndex').textContent);
    console.log(textContent);
    if (textContent) {
        upsertData({
            amount: textContent,
            dateCrawled: new Date()
        });
    }
    await browser.close();
};

function upsertData(data) {
    const DB_URL = 'mongodb://localhost/data';
    if (mongoose.connection.readyState == 0) {
        mongoose.connect(DB_URL);
    }
    let conditions = { dateCrawled: data.dateCrawled };
    let options = { upsert: true, new: true, setDefaultsOnInsert: true };
    Data.findOneAndUpdate(conditions, data, options, (err, result) => {
        if (err) throw err;
        if (result) {
            console.log("saved: " + result);
        }
    });
}

let j = schedule.scheduleJob('*/8 * * * * *', function () {
    console.log('save at' + new Date());
    run();
});
